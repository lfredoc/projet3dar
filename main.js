import * as THREE from 'three';
import { ARButton } from 'three/addons/webxr/ARButton.js';
import {
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader.js';
import { Capsule } from 'three/examples/jsm/math/Capsule.js';

import { OctreeHelper } from 'three/examples/jsm/helpers/OctreeHelper.js';
import { Octree } from 'three/examples/jsm/math/Octree.js';

import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';
let container;
let camera, scene, renderer;
let controller;
let mixer = 0;
let mixer2 = 0;

const clock = new THREE.Clock();
const STEPS_PER_FRAME = 5;
const GRAVITY = 1;

let reticle;

let hitTestSource = null;
let hitTestSourceRequested = false;
const capsuleHeight = 0.002;
const worldOctree = new Octree();

let front = false;
let back = false;
let right = false;
let left = false;

let visualPlayer;
let playerOnFloor = false;
const playerCollider = new Capsule(new THREE.Vector3(-0.005, 0.07, 0.139), new THREE.Vector3(-0.005, 0.07 + capsuleHeight, 0.139), 0.35);

const playerVelocity = new THREE.Vector3();

init();
animate();

function init() {

  container = document.createElement('div');
  document.body.appendChild(container);

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 20);

  const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
  directionalLight.position.set(- 5, 25, - 1);
  directionalLight.castShadow = true;
  directionalLight.shadow.camera.near = 0.01;
  directionalLight.shadow.camera.far = 500;
  directionalLight.shadow.camera.right = 30;
  directionalLight.shadow.camera.left = - 30;
  directionalLight.shadow.camera.top = 30;
  directionalLight.shadow.camera.bottom = - 30;
  directionalLight.shadow.mapSize.width = 1024;
  directionalLight.shadow.mapSize.height = 1024;
  directionalLight.shadow.radius = 4;
  directionalLight.shadow.bias = - 0.00006;
  scene.add(directionalLight);
  //
  const fillLight1 = new THREE.HemisphereLight(0x4488bb, 0x002244, 0.5);

  fillLight1.position.set(2, 1, 1);
  scene.add(fillLight1);
  //const light = new THREE.HemisphereLight(0xf9e59a, 0xacc9d3, 1);
  const light = new THREE.AmbientLight(0xffffff, 1); // soft white light
  scene.add(light);

  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.xr.enabled = true;
  container.appendChild(renderer.domElement);

  //

  document.body.appendChild(ARButton.createButton(renderer, { requiredFeatures: ['hit-test'] }));
  //

  const geometry = new THREE.CapsuleGeometry(0.002, capsuleHeight, 4, 8);
  const material = new THREE.MeshBasicMaterial();
  material.transparent = true;
  material.opacity = 0.0;

  //
  const game = new THREE.Object3D();
  game.environment = new THREE.TextureLoader().load("assets/Texture-base_baseColor.jpeg")
  game.updateMatrixWorld(true);


  visualPlayer = new THREE.Mesh(geometry, material);
  game.add(visualPlayer);
  visualPlayer.position.set(-0.005, 0.07, 0.139);



  function loadData() {
    const worldLoader = new GLTFLoader();
    worldLoader.setPath('assets/').load('livre.glb', gltfReader);
  }
  function dumpObject(obj, lines = [], isLast = true, prefix = '') {
    const localPrefix = isLast ? '└─' : '├─';
    lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);
    const newPrefix = prefix + (isLast ? '  ' : '│ ');
    const lastNdx = obj.children.length - 1;
    obj.children.forEach((child, ndx) => {
      const isLast = ndx === lastNdx;
      dumpObject(child, lines, isLast, newPrefix);
    });
    return lines;
  }

  function gltfReader(gltf) {
    let testModel = null;
    testModel = gltf.scene;

    if (testModel != null) {
      console.log("Model loaded:  " + testModel);
      game.add(gltf.scene);

      mixer = new THREE.AnimationMixer(testModel);
      if (gltf.animations.length) {
        mixer.clipAction(gltf.animations[0]).play();
        animate();
      }
      console.log(dumpObject(testModel).join('\n'));
      worldOctree.fromGraphNode(gltf.scene);

      gltf.scene.traverse(child => {

        if (child.isMesh) {

          child.castShadow = true;
          child.receiveShadow = true;

          if (child.material.map) {

            child.material.map.anisotropy = 4;
          }
        }

      });

      const helper = new OctreeHelper(worldOctree);
      helper.visible = false;
      game.add(helper);

      const gui = new GUI({ width: 200 });
      gui.add({ debug: false }, 'debug')
        .onChange(function (value) {

          helper.visible = value;

        });
    } else {
      console.log("Load FAILED.  ");
    }
  }
  loadData();

  {
    const catLoader = new GLTFLoader();
    catLoader.setPath('assets/').load('perso.glb', (gltf) => {
      const root = gltf.scene;
      root.position.y = root.position.y;

      if (root != null) {
        console.log("Model loaded:  " + root);
        visualPlayer.add(root)

        mixer2 = new THREE.AnimationMixer(root);
        if (gltf.animations.length) {
          mixer2.clipAction(gltf.animations[0]).play();
          animate();
        }
      }

    });
  }
  let bookPosition = new THREE.Vector3();
  let gameAbsent = true;
  let click = new THREE.Object3D();
  let vectorMove = new THREE.Vector3();

  function onSelect() {
    //fonction qui gère le click sur l'écran
    if (reticle.visible && gameAbsent) {
      // si le livre n'est pas déjà posé, on le crée.
      reticle.matrix.decompose(game.position, game.quaternion, game.scale);
      bookPosition.copy(game.position);
      scene.add(game);
      gameAbsent = false;
    }
    else {
      // sinon on gère les cas de déplacements du personnage.

      if (left == true || right == true || front == true || back == true) {
        // si le personnage est djà en déplacement on le stop
        front = false;
        back = false;
        right = false;
        left = false;
        //console.log("left : " + left + ' right : ' + right + ' front : ' + front+ ' back : ' + back);
      }
      else {
        // sinon on calcul son vecteur de déplacement pour connaitre la direction qu'il doit prendre
        reticle.matrix.decompose(click.position, click.quaternion, click.scale);
        vectorMove.copy(bookPosition);
        vectorMove.sub(click.position);

        //droite ou gauche a été choisi
        if (Math.abs(vectorMove.x) > Math.abs(vectorMove.z)) {
          if (vectorMove.x > 0) { left = true; }
          else { right = true; }
        }
        //devant ou derrière a été choisi
        else {
          if (vectorMove.z > 0) { front = true; }
          else { back = true; }
        }
        //console.log("left : " + left + ' right : ' + right + ' front : ' + front+ ' back : ' + back);

      }
    }
  }

  controller = renderer.xr.getController(0);
  controller.addEventListener('select', onSelect);
  scene.add(controller);

  reticle = new THREE.Mesh(
    new THREE.RingGeometry(0.15, 0.2, 32).rotateX(- Math.PI / 2),
    new THREE.MeshBasicMaterial()
  );
  reticle.matrixAutoUpdate = false;
  reticle.visible = false;
  scene.add(reticle);

  //

  window.addEventListener('resize', onWindowResize);

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);

}

function playerCollisions() {

  const result = worldOctree.capsuleIntersect(playerCollider);

  playerOnFloor = false;

  if (result) {

    playerOnFloor = result.normal.y > 0;

    if (!playerOnFloor) {

      playerVelocity.addScaledVector(result.normal, - result.normal.dot(playerVelocity));

    }

    playerCollider.translate(result.normal.multiplyScalar(result.depth));

  }

}
//

function updatePlayer(deltaTime) {

  let damping = Math.exp(- 4 * deltaTime) - 1;

  if (!playerOnFloor) {

    playerVelocity.y -= GRAVITY * deltaTime;

    // small air resistance
    damping *= 0.2;

  }

  playerVelocity.addScaledVector(playerVelocity, damping);

  const deltaPosition = playerVelocity.clone().multiplyScalar(deltaTime);
  playerCollider.translate(deltaPosition);

  playerCollisions();


}

function animate() {


  renderer.setAnimationLoop(render);

}

function render(timestamp, frame) {
  const deltaTime = Math.min(0.05, clock.getDelta()) / STEPS_PER_FRAME;
  mixer.update(deltaTime);
  mixer2.update(deltaTime);

  //changement de positiobn du personnage en fonctino de la direction choisie
  if (visualPlayer) {
    if (front) {
      visualPlayer.position.z = visualPlayer.position.z - 0.001

    }
    if (back) {
      visualPlayer.position.z = visualPlayer.position.z + 0.001
    }
    if (left) {
      visualPlayer.position.x = visualPlayer.position.x - 0.001
    }
    if (right) { visualPlayer.position.x = visualPlayer.position.x + 0.001 }
  }

  for (let i = 0; i < STEPS_PER_FRAME; i++) {

    updatePlayer(deltaTime);
  }

  if (frame) {

    const referenceSpace = renderer.xr.getReferenceSpace();
    const session = renderer.xr.getSession();

    if (hitTestSourceRequested === false) {

      session.requestReferenceSpace('viewer').then(function (referenceSpace) {

        session.requestHitTestSource({ space: referenceSpace }).then(function (source) {

          hitTestSource = source;

        });

      });

      session.addEventListener('end', function () {

        hitTestSourceRequested = false;
        hitTestSource = null;

      });

      hitTestSourceRequested = true;

    }

    if (hitTestSource) {

      const hitTestResults = frame.getHitTestResults(hitTestSource);

      if (hitTestResults.length) {

        const hit = hitTestResults[0];

        reticle.visible = true;
        reticle.matrix.fromArray(hit.getPose(referenceSpace).transform.matrix);
      } else {
        reticle.visible = false;
      }
    }
  }
  renderer.render(scene, camera);

}